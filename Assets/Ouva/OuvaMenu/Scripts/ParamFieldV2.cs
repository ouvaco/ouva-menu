﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class ParamFieldV2 : Param
    {
        public UnityEngine.UI.Text Title;
        public UnityEngine.UI.Text Description;
        public UnityEngine.UI.InputField InputField1;
        public UnityEngine.UI.InputField InputField2;

        public Vector2 v2() => new Vector2(float.Parse(InputField1.text), float.Parse(InputField2.text));

        public void Set(Vector2 v) {
            InputField1.text = v.x.ToString();
            InputField2.text = v.y.ToString();
        }
    }
}
