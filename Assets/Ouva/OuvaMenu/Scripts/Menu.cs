﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Ouva.GUI
{
    public class Menu : MenuItem
    {
        public ContentView ContentView;
        public Sidebar Sidebar;
        public RectTransform MenuPanel;

        [Ouva.GUI.Description("Directory to save/load config files.")]
        public string Directory = "config";

        [Ouva.GUI.Description("Increase/decrease the scaling factor of the menu window.")]
        public float ScaleFactor = 1f;

        public List<Section> Sections;

        [System.Serializable]
        public class Section {
            public string Name;
            public List<MenuItem> MenuItems;
        }

        public override void UpdateParameters (Ouva.GUI.SectionBody contentBody) {
            ScaleFactor = contentBody.f(this, "ScaleFactor");
        }

        // We're saving and loading window location and size
        // here without allowing it to be controlled from the menu
        protected override Dictionary<string, object> GetDictionary () {
            return new Dictionary<string, object>  {
                {"ScaleFactor", ScaleFactor},
                {"WindowLocation", MenuPanel.anchoredPosition},
                {"WindowSize", MenuPanel.sizeDelta}
            };
        }

        protected override void SetFromDictionary(Dictionary<string, object> dict) {
            ScaleFactor = (float) dict["ScaleFactor"];
            MenuPanel.anchoredPosition = (Vector2) dict["WindowLocation"];
            MenuPanel.sizeDelta = (Vector2) dict["WindowSize"];
        }

        // Start is called before the first frame update
        void Start()
        {
            ReloadMenu();
        }

        public void OnClick (int index)
        {
            ContentView.ActivateSection(index);
            ContentView.ScrollRect.verticalScrollbar.value = 1f;
        }

        public void SaveMenu () {
            Debug.Log("Menu > Saving...");

            foreach (var section in Sections)
                foreach (var item in section.MenuItems)
                    item.Save(Directory);
            

            Debug.Log("Menu > Saved.");
        }

        public void ReloadMenu () {
            Debug.Log("Menu > Loading...");

            foreach (var section in Sections)
                foreach (var item in section.MenuItems)
                    item.Load(Directory);

            Debug.Log("Menu > Loaded.");

            Layout();
        }

        void Layout () {
            Debug.Log("Menu > Laying out menus.");

            Sidebar.Clear();
            ContentView.Clear();

            foreach (var section in Sections)
            {
                Sidebar.AddSidebar(section.Name);
                ContentView.AddSection(section);
            }

            Sidebar.SetSelected(0);
            OnClick(0);

            Debug.Log("Menu > Layout ready.");
        }

        void Update () {
            GetComponent<UnityEngine.UI.CanvasScaler>().scaleFactor = ScaleFactor;
        }
    }
}
