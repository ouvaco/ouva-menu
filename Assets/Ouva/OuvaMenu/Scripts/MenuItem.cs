﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using Newtonsoft.Json;

namespace Ouva.GUI
{
    public abstract class MenuItem : MonoBehaviour
    {
        [Title]
        public string MenuTitle = "<Enter Menu Title>";

        public abstract void UpdateParameters (SectionBody contentBody);
        
        protected abstract Dictionary<string, object> GetDictionary ();
        protected abstract void SetFromDictionary (Dictionary<string, object> dict);

        static string SafePath(string path)
        {
            return path.ToLower().Replace(' ', '_').Replace(":", "");
        }

        public void Save (string directory) {
            var dict = GetDictionary();
            var data = new Dictionary<string, object>();

            // Add here custom json conversions.
            foreach (var item in dict)
            {
                // Save Vector2 as array
                if (item.Value.GetType() == typeof(Vector2))
                    data[item.Key] = new float[] { ((Vector2)item.Value).x, ((Vector2)item.Value).y};

                // Save Vector3 as array
                else if (item.Value.GetType() == typeof(Vector3))
                    data[item.Key] = new float[] { ((Vector3)item.Value).x, ((Vector3)item.Value).y, ((Vector3)item.Value).z};

                // Save rest as themselves by default.
                else data[item.Key] = item.Value;
            }

            Directory.CreateDirectory(directory);

            // Append to file
            using (FileStream fs = new FileStream(directory + "/" + SafePath(MenuTitle) + ".json", FileMode.OpenOrCreate, FileAccess.Write))
            {
                Debug.Log(string.Format("Menu {0}", MenuTitle) + " > Saving to file " + Path.GetFullPath(fs.Name));
                fs.SetLength(0);

                using (StreamWriter sw = new StreamWriter(fs))
                    sw.Write(JsonConvert.SerializeObject(data));
            }
        }

        public void Load (string directory) {
            //Debug.Log(string.Format("{0} GUI", transform.name) + " > Loading...");
            string json = null;

            try
            {
                
                using (FileStream fs = new FileStream(directory + "/" + SafePath(MenuTitle) + ".json", FileMode.Open, FileAccess.Read))
                {
                    Debug.Log(string.Format("Menu {0}", MenuTitle) + " > Loading from file " + Path.GetFullPath(fs.Name));

                    using (StreamReader sr = new StreamReader(fs))
                        json = sr.ReadToEnd();
                }

                if (json != null)
                {
                    var data = new Dictionary<string, object>();
                    JsonConvert.PopulateObject(json, data);

                    var parameters = GetDictionary();

                    foreach (var d in data)
                    {
                        if (!parameters.ContainsKey(d.Key))
                            continue;

                        // Fix for JsonConvert saving all ints as int64
                        if (data[d.Key].GetType() == typeof(System.Int64))
                            parameters[d.Key] = (int)(long) data[d.Key];
                        // Fix for JsonConvert saving all floats as doubles
                        else if (data[d.Key].GetType() == typeof(System.Double))
                            parameters[d.Key] = (float)(double) data[d.Key];
                        else if (data[d.Key].GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                        {
                            var arr = (Newtonsoft.Json.Linq.JArray)data[d.Key];

                            if (arr.Count == 2)
                                parameters[d.Key] = new Vector2((float)(double) arr[0], (float)(double) arr[1]);
                            else if (arr.Count == 3)
                                parameters[d.Key] = new Vector3((float)(double) arr[0], (float)(double) arr[1], (float)(double) arr[2]);
                        }
                        else
                            parameters[d.Key] = data[d.Key];
                    }

                    SetFromDictionary(parameters);
                }
            }
            catch (FileNotFoundException ex)
            {
                Debug.Log(string.Format("Menu {0}", MenuTitle) + " > Parameter file not found at: " + ex.FileName);
            }
            catch (DirectoryNotFoundException ex)
            {
                Debug.Log(string.Format("Menu {0}", MenuTitle) + " > Directory not found: " + ex.Message);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class Subtitle : System.Attribute {
        public string text {get; private set;}

        public Subtitle(string text) => this.text = text;
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class Title : System.Attribute {}

    // Fields with this class will be omitted
    // from menu layout. You can still save/load these if you need to.
    [AttributeUsage(AttributeTargets.Field)]
    public class Ignore : System.Attribute {}

    [AttributeUsage(AttributeTargets.Field)]
    public class Description : System.Attribute {
        public string text {get; private set;}

        public Description(string text) => this.text = text;
    }
}
