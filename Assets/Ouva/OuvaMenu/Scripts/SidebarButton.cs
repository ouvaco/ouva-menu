﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ouva.GUI
{
    public class SidebarButton : MonoBehaviour
    {
        public Font NormalFont;
        public Font PressedFont;
        public Text ButtonText;
        public Image Image;
        public Button Button;
        public Color TextActiveColor;

        private ColorBlock colorBlock;
        private Color normalTextColor;

        private bool initialized = false;

        private void Start() {
            Init();
        }

        private void Init () {
            if (initialized)
                return;

            colorBlock = Button.colors;
            normalTextColor = ButtonText.color;

            initialized = true;
        }
        
        public void SetActive(bool val) {
            Init ();

            ButtonText.font = val ? PressedFont : NormalFont;
            ButtonText.color = val ? TextActiveColor : normalTextColor;
            
            var colors = Button.colors;
            colors.normalColor = val ? Button.colors.pressedColor : colorBlock.normalColor;
            colors.highlightedColor = val ? Button.colors.pressedColor : colorBlock.highlightedColor;
            Button.colors = colors;
        }
    }
}