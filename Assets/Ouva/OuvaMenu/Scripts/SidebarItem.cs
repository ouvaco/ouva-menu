﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class SidebarItem : MonoBehaviour
    {
        public Sidebar Sidebar;

        public void SetTitle (string title) {
            GetComponentInChildren<UnityEngine.UI.Text>().text = title;
        }

        public void SetSelected () {
            GetComponentInChildren<SidebarButton>().SetActive(true);
        }

        public void OnClick() => Sidebar.OnClick(this);
    }
}

