﻿using UnityEngine;
using System.Collections;

namespace Ouva.GUI
{
    public class ToggleChild : MonoBehaviour {

        public Transform Child;
        public MonoBehaviour Component;
        public KeyCode Key;
        public bool StartOff = false;
        public bool HoldShift = false;

        private bool _turnedOff = false;
        
        // Update is called once per frame
        void Update () {

            if (StartOff && !_turnedOff)
            {
                if (Child)
                    Child.gameObject.SetActive(false);
                else if (Component)
                    Component.enabled = false;

                _turnedOff = true;
            }

            var shiftPressed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
            var modifierPassed = (HoldShift && shiftPressed) || (!HoldShift && !shiftPressed);

            if (!modifierPassed)
                return;

            if (Input.GetKeyUp(Key))
                Toggle();
        }

        public void Toggle ()
        {
            if (Child)
            {
                
                Child.gameObject.SetActive(!Child.gameObject.activeSelf);
                Debug.Log("ToggleChild > " + (Child.gameObject.activeSelf ? "Enabled" : "Disabled") + " " + Child.name);
            }
            else if (Component)
            {
                Component.enabled = !Component.enabled;
                Debug.Log("ToggleChild > " + (Component.enabled ? "Enabled" : "Disabled") + " " + Component.GetType() + " on " + Component.transform.name);
            }
        }
    }
}
