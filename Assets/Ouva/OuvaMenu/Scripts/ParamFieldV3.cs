﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class ParamFieldV3 : Param
    {
        public UnityEngine.UI.Text Title;
        public UnityEngine.UI.Text Description;
        public UnityEngine.UI.InputField InputField1;
        public UnityEngine.UI.InputField InputField2;
        public UnityEngine.UI.InputField InputField3;

        public Vector3 v3() => new Vector3(float.Parse(InputField1.text), float.Parse(InputField2.text), float.Parse(InputField3.text));

        public void Set(Vector3 v) {
            InputField1.text = v.x.ToString();
            InputField2.text = v.y.ToString();
            InputField3.text = v.z.ToString();
        }
    }
}
