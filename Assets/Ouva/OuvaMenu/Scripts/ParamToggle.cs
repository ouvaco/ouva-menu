﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class ParamToggle : Param
    {
        public UnityEngine.UI.Text Title;
        public UnityEngine.UI.Text Description;
        public UnityEngine.UI.Toggle Toggle;

        public bool b () {
            return Toggle.isOn;
        }
    }
}
