﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class Sidebar : MonoBehaviour
    {
        public Transform ItemsParent;
        public Transform ItemPrefab;
        public Menu Menu;
        
        public void AddSidebar (string title) {
            var item = Instantiate(ItemPrefab, ItemsParent);
            var sidebarItem = item.GetComponent<SidebarItem>();
            sidebarItem.Sidebar = this;
            sidebarItem.SetTitle(title);
        }

        public void Clear () {
            foreach (Transform c in ItemsParent)
                GameObject.Destroy(c.gameObject);
        }

        public void SetSelected(int index) => ItemsParent.GetChild(index).GetComponent<SidebarItem>().SetSelected();

        public void OnClick (SidebarItem item)
        {
            foreach (Transform i in ItemsParent)
            {
                if (i.gameObject.activeSelf && item != i)
                    i.GetComponentInChildren<SidebarButton>().SetActive(false);
            }

            item.GetComponentInChildren<SidebarButton>().SetActive(true);

            Menu.OnClick(item.transform.GetSiblingIndex());
        }
    }
}