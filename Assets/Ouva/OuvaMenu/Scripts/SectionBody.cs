﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;

namespace Ouva.GUI
{
    public class SectionBody : MonoBehaviour
    {
        public Transform ContentTitlePrefab;
        public Transform ContentSubtitlePrefab;
        public Transform ParamTogglePrefab;
        public Transform ParamFieldOnePrefab;
        public Transform ParamFieldV2Prefab;
        public Transform ParamFieldV3Prefab;

        private MenuItem _menuItem;
        private Menu.Section _section;
        private Dictionary<MenuItem, Dictionary<string, Param>> _parameters = new Dictionary<MenuItem, Dictionary<string, Param>>();

        public void Layout (Menu.Section section) {
            foreach (Transform c in transform)
                GameObject.Destroy(c.gameObject);

            foreach(var menuItem in section.MenuItems)
            {
                AddTitle(menuItem.MenuTitle);

                var fields = new Dictionary<string, System.Reflection.FieldInfo>();

                foreach (var field in menuItem.GetType().GetFields())
                    fields[field.Name] = field;

                _parameters[menuItem] = new Dictionary<string, Param>();

                foreach (var item in fields)
                {
                    var fieldName = item.Key;
                    var fieldType = item.Value.FieldType;
                    var fieldAttr = item.Value.GetCustomAttributes(false);
                    
                    var subtitleAttr = fieldAttr.FirstOrDefault(f => f.GetType() == typeof(Subtitle));
                    var subtitle = subtitleAttr != null ? ((Subtitle)subtitleAttr).text : null;

                    var descAttr = fieldAttr.FirstOrDefault(f => f.GetType() == typeof(Description));
                    var description = descAttr != null ? ((Description)descAttr).text : null;

                    var titleAttr = fieldAttr.FirstOrDefault(f => f.GetType() == typeof(Title));
                    var ignoreAttr = fieldAttr.FirstOrDefault(f => f.GetType() == typeof(Ignore));

                    // Derived class fields include base class parameter menu title,
                    // so here we skip it if it is the menu title.
                    if (titleAttr != null)
                        continue;

                    // Also we skip if parameter should be ignored
                    if (ignoreAttr != null)
                        continue;

                    if (subtitle != null)
                        AddSubtitle(subtitle);

                    if (fieldType == typeof(System.String))
                        _parameters[menuItem][fieldName] = AddFieldOne(fieldName, (string) item.Value.GetValue(menuItem), description, true);
                    else if (fieldType == typeof(System.Int32))
                        _parameters[menuItem][fieldName] = AddFieldOne(fieldName, ((int) item.Value.GetValue(menuItem)).ToString(), description, false, InputField.ContentType.IntegerNumber);
                    else if (fieldType == typeof(System.Single))
                        _parameters[menuItem][fieldName] = AddFieldOne(fieldName, ((float) item.Value.GetValue(menuItem)).ToString(), description, false, InputField.ContentType.DecimalNumber);
                    else if (fieldType == typeof(System.Boolean))
                        _parameters[menuItem][fieldName] = AddToggle(fieldName, (bool) item.Value.GetValue(menuItem), description);
                    else if (fieldType == typeof(Vector2))
                        _parameters[menuItem][fieldName] = AddFieldV2(fieldName, (Vector2) item.Value.GetValue(menuItem), description);
                    else if (fieldType == typeof(Vector3))
                        _parameters[menuItem][fieldName] = AddFieldV3(fieldName, (Vector3) item.Value.GetValue(menuItem), description);
                }
                
                _section = section;
            }
        }

        void AddTitle (string text) {
            var obj = Instantiate(ContentTitlePrefab, transform);
            obj.GetComponentInChildren<UnityEngine.UI.Text>().text = text;
        }

        void AddSubtitle (string text) {
            var obj = Instantiate(ContentSubtitlePrefab, transform);
            obj.GetComponentInChildren<UnityEngine.UI.Text>().text = text;
        }

        ParamFieldOne AddFieldOne (string title, string value, string description = null, bool isLong = true, InputField.ContentType ContentType = InputField.ContentType.Standard)
        {
            var obj = Instantiate(ParamFieldOnePrefab, transform);
            obj.name = title;
            var param = obj.GetComponentInChildren<ParamFieldOne>();

            param.SetWidth(isLong);
            param.SetContentType(ContentType);

            param.Title.text = title;

            if (description == null)
                param.Description.gameObject.SetActive(false);
            else
                param.Description.text = description;

            if (value != null)
                param.InputField.text = value;

            return param;
        }

        ParamFieldV2 AddFieldV2 (string title, Vector2 value, string description = null)
        {
            var obj = Instantiate(ParamFieldV2Prefab, transform);
            obj.name = title;
            var param = obj.GetComponentInChildren<ParamFieldV2>();

            param.Title.text = title;

            if (description == null)
                param.Description.gameObject.SetActive(false);
            else
                param.Description.text = description;

            param.Set(value);

            return param;
        }

        ParamFieldV3 AddFieldV3 (string title, Vector3 value, string description = null)
        {
            var obj = Instantiate(ParamFieldV3Prefab, transform);
            obj.name = title;
            var param = obj.GetComponentInChildren<ParamFieldV3>();

            param.Title.text = title;

            if (description == null)
                param.Description.gameObject.SetActive(false);
            else
                param.Description.text = description;

            param.Set(value);

            return param;
        }

        ParamToggle AddToggle (string title, bool value, string description = null) {
            var obj = Instantiate(ParamTogglePrefab, transform);
            obj.name = title;
            var param = obj.GetComponentInChildren<ParamToggle>();
            param.Title.text = title;

            if (description == null)
                param.Description.gameObject.SetActive(false);
            else
                param.Description.text = description;

            param.Toggle.isOn = value;

            return param;
        }

        private void Update() {
            foreach(var item in _section.MenuItems)
                item.UpdateParameters(this);
        }

        public int i(MenuItem menuRef, string paramName) => ((ParamFieldOne)_parameters[menuRef][paramName]).i();
        public float f(MenuItem menuRef, string paramName) => ((ParamFieldOne)_parameters[menuRef][paramName]).f();
        public Vector2 v2(MenuItem menuRef, string paramName) => ((ParamFieldV2)_parameters[menuRef][paramName]).v2();
        public Vector3 v3(MenuItem menuRef, string paramName) => ((ParamFieldV3)_parameters[menuRef][paramName]).v3();
        public string s(MenuItem menuRef, string paramName) => ((ParamFieldOne)_parameters[menuRef][paramName]).s();
        public bool b(MenuItem menuRef, string paramName) => ((ParamToggle)_parameters[menuRef][paramName]).b();
    }
}
