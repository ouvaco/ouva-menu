﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class ContentView : MonoBehaviour
    {
        public Transform ContentParent;
        public Transform ContentBodyPrefab;
        public UnityEngine.UI.ScrollRect ScrollRect;

        // Start is called before the first frame update
        void Start()
        {
            ActivateSection(0);
        }

        public void Clear () {
            foreach (Transform c in ContentParent)
                GameObject.Destroy(c.gameObject);
        }

        public void AddSection (Menu.Section section) {
            var item = Instantiate(ContentBodyPrefab, ContentParent);
            item.gameObject.SetActive(false);
            item.GetComponent<SectionBody>().Layout(section);
        }

        public void ActivateSection (int index) {
            for (int i = 0; i < ContentParent.childCount; i++)
                ContentParent.GetChild(i).gameObject.SetActive(i == index);
        }
    }
}
