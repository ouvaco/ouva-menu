﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ouva.GUI
{
    public class ParamFieldOne : Param
    {
        public UnityEngine.UI.Text Title;
        public UnityEngine.UI.Text Description;
        public UnityEngine.UI.InputField InputField;

        public int ShortWidth = 440;
        public int LongWidth = 900;

        public void SetWidth(bool isLong) => InputField.GetComponent<UnityEngine.UI.LayoutElement>().preferredWidth = isLong ? LongWidth : ShortWidth;
        
        public void SetContentType (UnityEngine.UI.InputField.ContentType type) => InputField.contentType = type;

        public int i() => int.Parse(InputField.text);
        public float f() => float.Parse(InputField.text);
        public string s() => InputField.text;
    }
}
