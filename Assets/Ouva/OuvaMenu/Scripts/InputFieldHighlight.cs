﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Ouva.GUI
{
    [RequireComponent(typeof(UnityEngine.UI.InputField))]
    public class InputFieldHighlight : MonoBehaviour
    {
        public Transform Highlight;

        private void Update() {
            Highlight.gameObject.SetActive(GetComponent<UnityEngine.UI.InputField>().isFocused);
        }

    }
}
