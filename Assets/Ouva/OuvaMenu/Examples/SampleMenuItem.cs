﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleMenuItem : Ouva.GUI.MenuItem
{
    [Ouva.GUI.Subtitle("Subtitle 1")]
    [Ouva.GUI.Description("This is a boolean field.")]
    public bool BoolField;
    
    [Ouva.GUI.Subtitle("Subtitle 2")]
    [Ouva.GUI.Description("This is a string field.")]
    public string StringField;

    [Ouva.GUI.Description("This is an int field.")]
    public int IntField;

    [Ouva.GUI.Description("This is a float field.")]
    public float FloatField;

    [Ouva.GUI.Description("This is a Vector2 field.")]
    public Vector2 Vector2Field;

    [Ouva.GUI.Description("This is a Vector3 field.")]
    public Vector3 Vector3Field;

    public override void UpdateParameters (Ouva.GUI.SectionBody contentBody) {
        BoolField = contentBody.b(this, "BoolField");
        StringField = contentBody.s(this, "StringField");
        IntField = contentBody.i(this, "IntField");
        FloatField = contentBody.f(this, "FloatField");
        Vector2Field = contentBody.v2(this, "Vector2Field");
        Vector3Field = contentBody.v3(this, "Vector3Field");
    }

    protected override Dictionary<string, object> GetDictionary () {
        return new Dictionary<string, object>  {
            {"BoolField", BoolField},
            {"StringField", StringField},
            {"IntField", IntField},
            {"FloatField", FloatField},
            {"Vector2Field", Vector2Field},
            {"Vector3Field", Vector3Field}
        };
    }

    protected override void SetFromDictionary(Dictionary<string, object> dict) {
        BoolField = (bool) dict["BoolField"];
        StringField = (string) dict["StringField"];
        IntField = (int) dict["IntField"];
        FloatField = (float) dict["FloatField"];
        Vector2Field = (Vector2) dict["Vector2Field"];
        Vector3Field = (Vector3) dict["Vector3Field"];
    }
}
