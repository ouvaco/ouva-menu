# ouva-menu

A menu solution for Unity, including auto-generation of multiple menus and save/load of config files.

Current Unity version is 2018.3.9f1 - developed in Linux (it should work on other platforms, just not tested yet.)

![](Assets/Ouva/OuvaMenu/Doc/menu-image-full-1.png)